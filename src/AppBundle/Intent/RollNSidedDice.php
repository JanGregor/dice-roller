<?php

namespace AppBundle\Intent;

use JanGregor\AlexaKitBundle\Common\IntentInterface;
use JanGregor\AlexaKitBundle\Model\Request\AlexaRequest;
use JanGregor\AlexaKitBundle\Model\Response\AlexaResponse;

class RollNSidedDice implements IntentInterface
{
    /**
     * @return string[]
     */
    public function getIntentNames(): array
    {
        return [
            'rollNSidedDice'
        ];
    }

    /**
     * @param AlexaRequest $request
     *
     * @return AlexaResponse
     */
    public function processRequest(AlexaRequest $request): AlexaResponse
    {
        $alexaResponse = new AlexaResponse();

        $max = intval($request->getRequest()->getIntent()->getSlots()['sides']->getValue());

        $alexaResponse->getResponse()->getOutputSpeech()->setText(sprintf(
            'I rolled a %s for you.',
            rand(1, $max)
        ));

        return $alexaResponse;
    }
}
