<?php

namespace AppBundle\Intent;

use JanGregor\AlexaKitBundle\Common\IntentInterface;
use JanGregor\AlexaKitBundle\Model\Request\AlexaRequest;
use JanGregor\AlexaKitBundle\Model\Response\AlexaResponse;

class RollSimpleDice implements IntentInterface
{
    /**
     * @return string[]
     */
    public function getIntentNames(): array
    {
        return [
            'rollSimpleDice'
        ];
    }

    /**
     * @param AlexaRequest $request
     *
     * @return AlexaResponse
     */
    public function processRequest(AlexaRequest $request): AlexaResponse
    {
        $alexaResponse = new AlexaResponse();

        $alexaResponse->getResponse()->getOutputSpeech()->setText(sprintf(
            'I rolled a %s for you.',
            rand(1,6)
        ));

        return $alexaResponse;
    }
}
